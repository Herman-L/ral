use super::Mode;
use num_bigint::BigInt;
use num_traits::Zero;
use std::convert::TryInto;
use std::io::{self, Bytes, Read, Stdin};

/// Helper struct used to read from stdin.
pub struct Reader {
    raw: RawReader,
    mode: Mode,
}

impl Reader {
    /// Creates a new reader.
    pub fn new(mode: Mode) -> Self {
        Self {
            raw: RawReader::new(),
            mode,
        }
    }
    /// Reads a single byte.
    fn read_byte(&mut self) -> Option<u8> {
        self.raw.next()
    }
    /// Reads a decmal number.
    fn read_decimal(&mut self) -> Option<BigInt> {
        let mut negative = false;
        loop {
            let b = self.raw.peek()?;
            if b.is_ascii_digit() {
                let mut num = BigInt::zero();
                while self.raw.peek().map_or(false, |b| b.is_ascii_digit()) {
                    num = num * 10 + (self.raw.next().unwrap() - b'0');
                }
                return Some(if negative { -num } else { num });
            }
            negative = b == b'-';
            self.raw.next();
        }
    }
    /// Reads a UTF-8 encoded character.
    fn read_utf8(&mut self) -> Option<char> {
        const ERR_CHAR: char = '\u{FFFD}';
        let first_byte = self.raw.next()?;
        let (mask, trailing) = match first_byte {
            0x00..=0x7F => (0xFF, 0),
            0xC0..=0xDF => (0x1F, 1),
            0xE0..=0xEF => (0x0F, 2),
            0xF0..=0xF7 => (0x07, 3),
            _ => return Some(ERR_CHAR),
        };
        let mut value = u32::from(first_byte) & mask;
        for _ in 0..trailing {
            if let Some(0x80..=0xBF) = self.raw.peek() {
                let byte = self.raw.next().unwrap();
                value = (value << 6) | u32::from(byte) & 0x3F;
            } else {
                return Some(ERR_CHAR);
            }
        }
        Some(value.try_into().unwrap_or(ERR_CHAR))
    }
    /// Reads a value from stdin.
    pub fn read(&mut self) -> BigInt {
        match self.mode {
            Mode::Byte => self.read_byte().map(BigInt::from),
            Mode::Decimal => self.read_decimal(),
            Mode::Utf8 => self.read_utf8().map(u32::from).map(BigInt::from),
        }
        .unwrap_or_else(BigInt::zero)
    }
}

/// Helper struct used by `Reader` to read and peek single bytes.
struct RawReader {
    reader: Bytes<Stdin>,
    peeked: Option<u8>,
}

impl RawReader {
    /// Creates a new `RawReader`.
    fn new() -> Self {
        Self {
            reader: io::stdin().bytes(),
            peeked: None,
        }
    }
    /// Peeks the next byte.
    fn peek(&mut self) -> Option<u8> {
        self.peeked = self.next();
        self.peeked
    }
    /// Reads the next byte.
    fn next(&mut self) -> Option<u8> {
        let value = self.peeked.take();
        value.or_else(|| self.reader.next().transpose().unwrap())
    }
}
