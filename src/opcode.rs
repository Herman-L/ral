/// A single opcode.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub enum Opcode {
    Zero,
    One,
    Add,
    Sub,
    Copy,
    Swap,
    Load,
    Store,
    Input,
    Output,
    Jump,
    Noop,
}

impl Opcode {
    /// Parses a byte as an opcode. Returns `None` if the byte is a comment (not an opcode).
    pub fn from_byte(b: u8) -> Option<Self> {
        match b {
            b'0' => Some(Self::Zero),
            b'1' => Some(Self::One),
            b'+' => Some(Self::Add),
            b'-' => Some(Self::Sub),
            b':' => Some(Self::Copy),
            b'/' => Some(Self::Swap),
            b'*' => Some(Self::Load),
            b'=' => Some(Self::Store),
            b',' => Some(Self::Input),
            b'.' => Some(Self::Output),
            b'?' => Some(Self::Jump),
            b'_' => Some(Self::Noop),
            _ => None,
        }
    }
    /// Creates an opcode vec from a byte iterator.
    pub fn from_iter(iter: impl Iterator<Item = u8>) -> Vec<Self> {
        iter.filter_map(Self::from_byte).collect()
    }
}
