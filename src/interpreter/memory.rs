use num_bigint::BigInt;
use num_traits::Zero;
use std::collections::HashMap;
use std::fmt::{self, Debug, Formatter};

/// The memory use by the `Interpreter`.
pub struct Memory {
    data: HashMap<BigInt, BigInt>,
}

impl Memory {
    /// Creates a new empty memory.
    pub fn new() -> Self {
        Self {
            data: HashMap::new(),
        }
    }
    /// Loads the value at the index. Returns `0` if it has not been written to.
    pub fn load(&self, index: &BigInt) -> BigInt {
        self.data.get(index).cloned().unwrap_or_else(BigInt::zero)
    }
    /// Stores a value at the specified index.
    pub fn store(&mut self, index: BigInt, value: BigInt) {
        if value.is_zero() {
            self.data.remove(&index);
        } else {
            self.data.insert(index, value);
        }
    }
}

impl Debug for Memory {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        Debug::fmt(&self.data, f)
    }
}
