use num_bigint::BigInt;
use num_traits::Zero;
use std::fmt::{self, Debug, Formatter};

/// The stack used by the `Interpreter`.
pub struct Stack {
    data: Vec<BigInt>,
}

impl Stack {
    /// Creates a new, empty stack.
    pub const fn new() -> Self {
        Self { data: Vec::new() }
    }
    /// Pushes a value to `self`.
    pub fn push(&mut self, value: BigInt) {
        // No need to store zero when the stack is empty.
        if !(self.data.is_empty() && value.is_zero()) {
            self.data.push(value);
        }
    }
    /// Pops a value from `self`. Returns zero if `self` is empty.
    pub fn pop(&mut self) -> BigInt {
        self.data.pop().unwrap_or_else(BigInt::zero)
    }
}

impl Debug for Stack {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        Debug::fmt(&self.data, f)
    }
}
