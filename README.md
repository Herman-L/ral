# Ral

Ral is a stack-based random-access esoteric language inspired by [Brain-Flak]
and [brainfuck], as well as the mix of stack operations and random access memory
used in [WebAssembly].

The main design goals are, in order:

1. Random accesses. Most esolangs are either completely stack based,
   complicated, or almost unusable. This is achived by the `*`, `=` and `?`
   opcodes, all of which use pointers. 
2. Simplicity. Ral uses a small number of opcodes (12 in total, compared to the
   41 used in [Labyrinth], for example). There also aren't any artificial
   restrictions like memory size or integer width.
3. Efficiency. The use of `>0` instead of `≠0` in the jump condition makes it
   possible to "binary search" the value of an integer, allowing division in
   O(log<sup>2</sup>n) operations, for example.

[Brain-Flak]: https://esolangs.org/wiki/Brain-Flak
[brainfuck]: https://esolangs.org/wiki/Brainfuck
[WebAssembly]: https://webassembly.org/
[Labyrinth]: https://esolangs.org/wiki/Labyrinth

## Description

Ral has a stack (known as "the stack") and a random-access memory (known as
"memory"). The stack is used as working memory by all commands. The stack 
and memory are initially filled with an infinite amount of zeros.

Similar to Brain-Flak and brainfuck, only a limited set of characters are
treated as opcodes. All other characters are treated as comments. The valid
opcodes in Ral are:

| Char | Operation                         | Description                    |
| :--: | --------------------------------- | ------------------------------ |
|  `0` | `Push 0`                          | Push `0` to the stack          |
|  `1` | `Push 1`                          | Push `1` to the stack          |
|  `+` | `Pop A` `Pop B` `Push A+B`        | Integer addition               |
|  `-` | `Pop A` `Pop B` `Push A-B`        | Integer subtraction            |
|  `:` | `Pop A` `Push A` `Push A`         | Duplicate the top of the stack |
|  `/` | `Pop A` `Pop B` `Push A` `Push B` | Swap the top of the stack      |
|  `*` | `Pop A` `Push Memory[A]`          | Read a value from memory       |
|  `=` | `Pop A` `Pop B` `Memory[A]=B`     | Write a value to memory        |
|  `,` | `Input A` `Push A`                | Input a value                  |
|  `.` | `Pop A` `Output A`                | Output a value                 |
|  `?` | `Pop A` `Pop B` `If B>0 Goto A`   | Conditional jump to `A`.       |
|  `_` | No-op                             | Opcode that does nothing       |

The jump opcode jumps to the `A`-th opcode in the program (zero-indexed),
ignoring comments. The `_` opcode can be used to pad jump destinations, as it
does nothing but does count as an opcode. For example, jumping to index `3` in
the following program jumps to the `+` opcode:

    0 foo 1_+ bar :

Jumping to negative indexes results in jumping to the start of the program and
jumping to indexes larger than the number of opcodes halts the program, as the
program counter reaches the end.

## Compiling the interpreter

1.  Install [Rust](https://rust-lang.org/tools/install).
2.  Compile the interpreter: `cargo build --release`
3.  Run the interpreter:
    -   Unix-like: `target/release/ral examples/helloworld.txt`
    -   Windows: `target\release\ral.exe examples\helloworld.txt`

## Interpreter usage

    USAGE:
        ral [FLAGS] <SOURCE>

    FLAGS:
        -b, --bytes      Use raw byte I/O
        -d, --decimal    Use decimal I/O
        -h, --help       Prints help information
        -u, --utf8       Use UTF-8 I/O (default)
        -V, --version    Prints version information

    ARGS:
        <SOURCE>    The source file
